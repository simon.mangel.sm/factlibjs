This documents explains the fundamental concepts behind the factlib.js library as well as core concepts of the library implementation itself.

## Theoretical Foundations and Technologies
While Factlib.js can be used without deep understanding of the underlying model, it is helpful to know at least the basic terminology used within the library and understand the various involved elements and protocols to some extent.

### The FactDAG Model
The [FactDAG](https://doi.org/10.1109/JIOT.2020.2966402) is a directed acyclic graph used to track provenance relations between facts and processes.
The important concepts of the FactDAG are as follows:
* Fact

    Facts are immutable revisions of data resources that can be reliably referenced by their persistant identifier, the FactID.

* FactID

    FactIDs are triples consisting of the authority responsible for the fact identified by the ID, an internalID, 
    identifying a resource within its authority and a revisionID identifying a specific revision of a resource. 
    Such triples uniquely identify facts.
    
* Process

    Processes can take facts as input and create new facts based on their input.

* Process Execution

    Process executions are instances of processes which are explicitly recorded.

* Authority

    Authorities are entities responsible for processes and facts in their domain. 
    For example, companies owning and controlling data may be modelled as authorities.     
    
* FactDAG
    
    The FactDAG is a directed acyclic graph that connects Facts and process executions and the influences between them. 
    The implementation uses the [PROV model](https://www.w3.org/TR/2013/NOTE-prov-primer-20130430/) to express these relations and track provenance.


<!--<img src="factdag.png" alt="FactDAG" width="600"/> -->

### FactDAG and PROV
<img src="factdag_prov.PNG" alt="FactDAG PROV" width="900"/>

As illustrated, the elements of the FactDAG and their relations are aligned with concepts of the W3C PROV-O provenance standard, allowing for standard-compliant recording of provenance information.
A key feature of Factlib.js is the automated creation of standard-compliant provenance information. 

### Server Technologies
Factlib.js is a client library that interacts with one or multiple servers each controlled by an authority.
The current implementation relies on a server compliant with the [Linked Data Platform](https://www.w3.org/TR/ldp/) specification 
and the [Memento Protocol](https://mementoweb.org/guide/rfc/), namely the [Trellis LDP](https://github.com/trellis-ldp/trellis).
This allows interoperable interactions between clients and servers using standardized protocols based on HTTP communication.
Each authority is assumed to control its own LDP instance which is used to store the resources and make the resources, as well as their immutable revisions avaiable through HTTP and the Memento protocol.

Additionally, authorities may maintain message brokers which allow clients (like FactDAG processes) to subscribe to new revisions of specific resources.
The [Deployment-Example](../examples/deploy) contains a Docker-Compose file that sets up such an environment consisting of multiple authorities, their LDPs and message brokers for development purposes.


## How Factlib.js functions internally
Factlib.js uses the introduced technologies and concepts to provide an abstract API for its users to read facts, write facts or subscribe to resources while automatically creating provenance information. 

### Resource Models
<img src="resources.PNG" width="900" alt="Resource Models"/>

The Factlib.js library models resource types as different classes according to the different FactDAG types.
There are two types of model classes, the Resources and the ResourceCandidates.
The library returns Resource objects as responses to requests to the server.
Resources contain a local RDF store that contains all triples from the resource as they exist on the server.
This store can be queried for its content, but a resource is immutable and cannot be modified. 
This approach has been chosen to reflect the immutability of facts in the FactDAG model.  
To modify a resource, a corresponding ResourceCandidate can be derived from the concrete subclasses of the resource.
For example, for an existing Fact `f`, the corresponding, mutable FactCandidate can be created by calling `f.createCandidate()`
The correct PROV type is automatically added to new candidates. 
Additionally, the PROV relation `prov:wasRevisionOf` is added to candidates that are directly derived from an Resource.
A derived candidate initially contains the same RDF data as the resource, but the triples in its store can be manipulated. 
In general, arbitrary manipulations to the RDF data are possible, but the different candidate classes provide predefined methods for the most common operations on their instances to simplify their usage.

<img src="candidate_cycles.PNG" width="400" alt="Candidate Cycles"/>

For creation and modification operations on resources, the library takes candidate objects and returns resource objects if the associated REST operation was successful.
The library transforms candidates to resources by sending them to the server.
The resource that is returned by the library also contains additional information, such as its FactID, referencing the Fact, which a candidate does not have.

#### Facts and Activities
In the implementation, there are different subclasses of the Fact class. 
The Broker class is used for facts that contain configuration information allowing clients to establish subscriptions to a specific message broker.
The Directory class can contain other resources that are retrievable using the `getContainedResourceList(): URL []` method of that class. 
The Process class models FactDAG processes.
It allows to retrieve the URL of the code repository that contains the source code of the associated process application using `getCodeRepository(): URL`.
The Authority class models FactDAG authorities. 
It can be used to learn where information about the message broker responsible for that authority is stored using `getMessageBrokerURI(): URL`.
Since objects of that class are also of the class Fact, according to the mapping previously introduced, they are all entities in the PROV model.

The only subclass of the Resource class which is not also a subclass of the Fact class, is the Activity.
Since the constraints for the PROV model explicitly state that activities and entities in the PROV model are disjoint, activities cannot be modeled as facts.
Therefore, an activity is modeled as a Resource, but not as a Fact.
Consequently, Activity objects do not contain the `a prov:Entity` triples, but only the `a prov:Activity` triple, to maintain compatibility with the PROV constraints.
Nevertheless, activity resources are uniquely identified and can be versioned like any other resource. 
The provided class model can be used to implement core features of the library like reading and writing facts. 

### Reading and Writing Facts

To read or write Facts to an LDP server, the LDPMementoClient can be used.
~~~
const service = new LdpMementoClient();
~~~
At its core, Factlib.js can be used to read and write facts. 
Full code examples on how to use the library are [part of this repository](../examples).
Facts can be retrieved by their __FactID__. 
LDP-based FactIDs consist of a domain name, a path to a resource and a timestamp to retrieve a specific revision.


A resource is identified by its __ResourceID__, which only consists of the authorityID and the internal resourceID.
A resourceID is an object that can be created by the IDFactory: 
~~~
const idFactory = service.getIDFactory();
const resourceID = idFactory.createResourceID("http://authority.com", "/path/to/resource");`
~~~

#### Creating a new resource
A new resource candidate can be created from an RDF string in Turtle serialization and a resourceID that points to a resource that does not yet exist.
This candidate is the representation of a resource state that is not (yet) persisted to the server backend.
~~~
const content = "@prefix rdf: [...]";
const candidate = new FactCandidate(content, resourceID);
~~~
Such a candidate can be persisted to the server. This requests returns the persisted representation of the resulting Fact.
~~~
fact = await service.createFactResource(candidate);
~~~
This method of directly creating resources offers a large degree of flexibility and control. 
However, this method **does not** automatically create the associated provenance elements as described by the FactDAG.
The user must add the necessary Process- and Activity-Facts manually and connect them correctly to the created facts, as illustrated in the [Binary Example](../examples/binaryExample/binaryExample.ts)
As an alternative, the ProcessLifecycleManager offers less control, but increased automation, as discussed later on. 

#### Updating an existing Resource
A resourceID can be used to retrieve the latest revision of the corresponding resource: 
~~~
const fact = await service.getLastFactRevision(resourceID);
~~~

The resulting Fact object contains an RDF store which holds the content of the Fact resource.
This store may be serialized or queried using the underlying RDF library [rdflib.js](https://github.com/linkeddata/rdflib.js/).
For example, the store may be queried for triples containing a specific predicate. 
Namespaces can be created to aid and organize such queries, as specified by rdflib.js.
~~~
const factstring = fact.serialize();
const DC = new rdf.Namespace("http://purl.org/dc/elements/1.1/");
const price = fact.queryStore(DC("price"));
~~~

The result of a query can then be parsed further (see the [Book Example](../examples/books/publisher/auth1/publishBooks.ts) and rdflib.js). 
Since Facts are considered immutable and reflect a specific revision of a resource, their store should not be directly manipulated.
Instead, a mutable candidate should be derived from the fact.
~~~
const candidate = fact.createCandidate();
~~~

The store of this candidate may then be manipulated with various rdflib.js methods, reflecting the state of a resource that has not been persisted yet.
For example, a specific triple may be added to the candidate. 
~~~
const res = candidate.store.sym(resourceID.resourceURI.toString());
candidate.store.add(res, DC("price"), 50, res.doc());
~~~ 

The updated candidate can be persisted afterwards. 
~~~
newFact = await service.createFactRevision(candidate);
~~~
This operation returns a new, immutable Fact as it was persisted to the server.


### Handling Binaries
All provenance information is handled as RDF data by Factlib.js. 
Ideally, the handled data is in RDF format as well.
However, some resources (like images) are not in RDF format. 
Factlib.js also handles these _binaries_, as illustrated in the [Binary Example](../examples/binaryExample/binaryExample.ts).

#### Reading Binaries
Known binaries can be requested from a server via the library.
~~~
const binaryFact = await service.getLastBinaryFactRevision(resourceID, "image/png");
~~~
The user can handle the resulting stream as she wishes. 
For example, the binary can be written to the local filesystem
~~~
binaryFact.binaryContent.pipe(fs.createWriteStream("./output/logo.png"));
~~~

#### Creating Binaries
To create a new binary resource, the user can (for example) stream a local file to a new BinaryFactCandidate, by using the FactDagResourceFactory and passing the resourceID, the stream itself and the contentType of the data.
~~~
const candidate = service.getFactDagResourceFactory().createEmptyBinaryFactCandidate(resourceID, fs.createReadStream("./output/logo.png"), "image/png");
~~~
This candidate can be persisted as a BinaryFact.
~~~
const fact = await service.createBinaryFactResource(candidate);
~~~

### Process Lifecycle Manager
The ProcessLifecycleManager(PLM) automatically creates the provenance triples, and resources that are required to maintain a full and compliant FactDAG.
However, its use is currently limited to specific, statically defined scenarios.
The PLM has to be initialized with a [configuration file](../examples/books/publisher/auth1/config.json).
This configuration file defines the resource locations for all resources associated with a process and its execution. 
Specifically, the authority responsible for the process, the activity resource and the process resource. 
Additionally, it defines the resources that are used by the process. 
These used resources are later automatically referenced in the activity resource revision using PROV.
~~~
{
    "authority": {
        "uri": "http://localhost:8081/"
    },
    "process": {
        "checkLatest": false,
        "location": "/processes/handleBinariesExample",
        "uses": []
    },
    "activity": {
        "location": "/activities/handleBinariesExample"
    }
}
~~~
The PLM can be initialized with the configuration and started.
~~~
import * as config from "./config.json";
const pManager = new ProcessLifecycleManager(config);
await pManager.start();
~~~
Now, one or multiple candidates (such as `c1` and `c2`) can be passed to the PLM instead of writing them directly to the server.
~~~
await pManager.execProcess([c1,c2]);
~~~
The PLM persists the candidates to one or multiple servers and uses the provided configuration to integrate the resulting facts in a compliant FactDAG.
Specifically, this entails that the PLM creates a process resource, linked with PROV to its responsible authority.
For each execution using the `execProcess` method, the PLM also creates revisions of the configured activity resource.
This activity is linked to the current revision of the process resource, and references the resources that are configured as used.
Subsequently, the candidates are linked to this activity (by referencing it using PROV) before they are persisted.
This guarantees a complete and compliant FactDAG.

### Subscriptions
The PLM can also be used to subscribe to new revisions of specific resources and automatically trigger a process execution if a relevant resource changes.+
This requires the authorities responsible for these facts to publish changes to their resources to a message broker.
This message broker must be described by a `MessageBroker` fact which must be referenced by the authority fact.
Such a message broker reference is created by the [Setup-Script](../examples/books/setup/setup.ts) for the Book-Example. 

<img src="subscriptions-Page-1.png" alt="Subscriptions" width="600"/>

The library can then establish a connection to one or multiple message brokers and subscribe to the resources which are configured as dependencies (used).
The user of the library can enable subscriptions by passing a callback function to the PLM when it is stared.
~~~
pManager.start(async (resource) => {
       // Do something
});
~~~
The PLM triggers this callback function when a resource configured as used changed and directly passes the new resource revision to the client application.
The client application can then react to the changed input, for example by recalculating its output.
The PLM always holds all current revisions of the configured dependency resources which can be retrieved by the used from the local store. 
~~~
const dependency = pManager.dependencies.get("http://authority.com/resource");
~~~

### Process Revisions
Finally, Factlib.js also considers revision of process resources.

<img src="process_versions.PNG" alt="Subscriptions" width="600"/>

To record the complete provenance of facts, not only the data that influenced a fact is important, but also the process logic that created a fact from its influences. 
The [Process Pipeline Example](../examples/processPipeline) provides scripts that automatically create a new revision of an associated process resource which references the latest commit in the process code repository. 
This way, the execution of a process is always linked to a specific commit which can be used to look up the exact code of that process.
To gurantee, that the code that is actually executed is the same revision that is referenced in the current process resource revision, the pipeline creates a `commit.json` file in the the repository.
This file can be used by Factlib.js to verify, that the commit hash contained in the file is identical to the commit hash currently referenced in the process resource on the server of the responsible authority.
Otherwise, the PLM does not execute processes. 
This behavior can be activated by setting `"checkLatest": true` in the configuration file.

