import {Namespaces} from "@i5/factlib-utils";
import {FactCandidate} from "../candidates/factCandidate";
import {FactDagResource} from "./factDagResource";
import {IFactID} from "../../interfaces/IFactID";
import {IResourceID} from "../../interfaces/IResourceID";

/**
 * Representation of a FactDAG-Fact. <br />
 * A fact is an immutable data point that is stored in a Linked Data Platform. <br />
 * All Facts are [[Resource]]s. <br />
 * A Fact is a PROV:Entity in the PROV Model and it can also contain records of the [[Activity]] that created it and
 * the [[Authority]] that is responsible for it. <br />
 * They are identified by the [[Authority]] they exist under, their resource URI and a timestamp. <br />
 * Facts can't be modified, but their content can be queried and a mutable [[FactCandidate]] can be derived. <br />
 * [[Process]] and [[Authority]] are Facts themselves.
 */
export class Fact<TTrunk = any> extends FactDagResource<TTrunk> {

    constructor(store: any, factID: IFactID, authorityResourceID: IResourceID, trunk: TTrunk) {
        super(store, factID, authorityResourceID, trunk);
    }

    /**
     * Retrieves the authority that is prov:associatedWith the fact from the local store.
     * @returns the URL of the authority that is responsible for this fact.
     */
    public getResponsibleAuthorityURI(): string {
        const rdfResult = this.queryStore(Namespaces.PROV(Namespaces.PROV_RELATION.WAS_ASSOCIATED_WITH));
        return rdfResult[0].object.value;
    }

    /**
     * Retrieves the activity that this fact was prov:generatedBy from the local store.
     * @returns the URI of the activity that generated this fact.
     */
    public getGeneratingActivityURI(): string {
        const rdfResult = this.queryStore(Namespaces.PROV(Namespaces.PROV_RELATION.WAS_GENERATED_BY));
        return rdfResult[0].object.value;
    }

    /**
     * Generates a mutable [[FactCandidate]] containing the data from the current, immutable Fact.
     */
    public createCandidate(): FactCandidate<TTrunk> {
        const candidate = new FactCandidate(this.serialize(), this.resourceID, this.trunk);
        candidate.clearProv();
        candidate.addEntityProv();
        candidate.wasRevisionOf(this);
        candidate.attributeToAuthority(this.authorityResourceID);
        return candidate;
    }
}
