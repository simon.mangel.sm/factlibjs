/**
 * Representation of a Resource as it immutably exists in an LDP.
 */
import {Resource} from "../../resource";
import {IFactID} from "../../interfaces/IFactID";
import {IResourceID} from "../../interfaces/IResourceID";

export abstract class FactDagResource<TTrunk = any> extends Resource<TTrunk> {
    private _factID: IFactID;
    private _authorityResourceID: IResourceID;

    protected constructor(store: any, factID: IFactID, authorityResourceID: IResourceID, trunk: TTrunk) {
        super(store, factID, trunk);
        this._factID = factID;
        this._authorityResourceID = authorityResourceID;
    }

    get authorityResourceID(): IResourceID {
        return this._authorityResourceID;
    }

    get factID(): IFactID {
        return this._factID;
    }
}
