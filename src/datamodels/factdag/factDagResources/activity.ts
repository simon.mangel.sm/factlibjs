import {ActivityCandidate} from "../candidates/activityCandidate";
import {FactDagResource} from "./factDagResource";
import {IFactID} from "../../interfaces/IFactID";
import {IResourceID} from "../../interfaces/IResourceID";

/**
 * Representation of a FactDAG-Process execution (Activity).
 * Every time a Process is executed, a new revision of the associated activity should be created.
 * An activity can record the [[Fact]]s that were used in the execution and the associated [[Process]] in PROV format.
 */
export class Activity<TTrunk = any> extends FactDagResource<TTrunk> {

    constructor(store: any, factID: IFactID, authorityResourceID: IResourceID, trunk: TTrunk) {
        super(store, factID, authorityResourceID, trunk);
    }

    /**
     * Generates a mutable [[activityCandidate]] containing the data from the current, immutable Activity-Revision.
     */
    public createCandidate(): ActivityCandidate<TTrunk> {
        const candidate = new ActivityCandidate<TTrunk>(this.serialize(), this.resourceID, this.trunk);
        candidate.clearProv();
        candidate.addActivityProv();
        return candidate;
    }

}
