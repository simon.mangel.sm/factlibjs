/**
 * Representation of a Directory containing a list of resources and directories.
 * Authorities are responsible for the Facts that are associated with them.
 */
import {IResourceID} from "../../interfaces/IResourceID";

export class Directory<TTrunk = any> {
    constructor(private resourceID: IResourceID,
                private resourceList: IResourceID[],
                private directoryList: IResourceID[],
                private trunk: TTrunk) {}

    /**
     * Retrieves the resources contained in the directory.
     * @return An array containing the ResourceIDs of all the resources that are contained in the directory.
     */
    public getContainedResourceList(): IResourceID[] {
        return this.resourceList;
    }

    /**
     * Retrieves a list of the ResourceIDs belonging to directories contained in this directory.
     * @return List of the ResourceIDs belonging to directories contained in this directory.
     */
    public getContainedDirectoriesList(): IResourceID[] {
        return this.directoryList;
    }
}
