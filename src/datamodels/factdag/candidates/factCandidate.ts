import {Namespaces} from "@i5/factlib-utils";
import {Activity} from "../factDagResources/activity";
import {Fact} from "../factDagResources/fact";
import {ResourceCandidate} from "./resourceCandidate";
import {EmptyTrunk} from "../../../factClient/EmptyTrunk";
import {IResourceID} from "../../interfaces/IResourceID";

/**
 * Representation of a FactCandidate. <br />
 * Facts are immutable and they are always represented, as they are stored in the backend <br />
 * A FactCandidate is similar to a Fact, but mutable. <br />
 * To update a Fact in an LDP, it has to be retrieved using the [[Service]] and converted into a FactCandidate
 *  (or a derived [[ProcessCandidate]]  or [[AuthorityCandidate]])) <br />
 * The Candidate can be modified and handed to the [[Service]], that updates the Resource in the LDP and
 *  (if successful) returns a new, immutable Fact.
 */
export class FactCandidate<TTrunk = EmptyTrunk> extends ResourceCandidate<TTrunk> {

    constructor(store: any, resourceID?: IResourceID, trunk?: TTrunk) {
        super(store, resourceID, trunk);
        this.addEntityProv();
    }

    /**
     * Locally attributes the factCandidate to an authority.
     * Every fact should be attributed to the authority that it exists under.
     * @param authorityID The ResourceID that the fact should be linked to.
     */
    public attributeToAuthority(authorityID: IResourceID): void {
        this.replaceProvenanceRelation(Namespaces.PROV_RELATION.WAS_ATTRIBUTED_TO, authorityID.factURI);
    }

    /**
     * Replaces all existing prov:wasRevisionOf triples from the factCandidate in the local store
     * with a prov:wasRevisionOf triple linking to the given Fact. If a fact has a multiple, previous revisions,
     * only the direct predecessor should be recorded.
     * @param fact The URI (usually at the root of a domain) of the authority that the fact should be linked to.
     */
    public wasRevisionOf(fact: Fact<TTrunk>): void {
        this.replaceProvenanceRelation(Namespaces.PROV_RELATION.WAS_REVISION_OF, fact.factID.factURI);
    }

    /**
     * Writes the rdf:type prov:Entity locally to the current factCandidate.
     * This should be present for every fact.
     */
    public addEntityProv(): void {
        this.addProvenanceType(Namespaces.PROV_TYPE.ENTITY);
    }

    /**
     *  Records the activity that created the fact (or is going to create the fact from the candidate).
     * This should be set after the activity has been successfully created in an LDP with the returned URI and
     * before the factCandidate is transmitted to an LDP itself.
     * @param activity the activity object that describes the execution of the process that generated this fact.
     */
    public addGeneratingActivity(activity: Activity<TTrunk>): void {
        this.replaceProvenanceRelation(Namespaces.PROV_RELATION.WAS_GENERATED_BY, activity.factID.factURI);
    }
}
