import {Namespaces} from "@i5/factlib-utils";
import {Authority} from "../factDagResources/authority";
import {FactCandidate} from "./factCandidate";
import {EmptyTrunk} from "../../../factClient/EmptyTrunk";
import {IFactID} from "../../interfaces/IFactID";

/**
 * Representation of a ProcessCandidate. <br />
 * See also: [[FactCandidate]] and [[Process]]
 */
export class ProcessCandidate<TTrunk = EmptyTrunk> extends FactCandidate<TTrunk> {

    constructor(store: any, factID: IFactID, trunk?: TTrunk) {
        super(store, factID, trunk);
        this.addProcessProv();
    }

    /**
     * Writes the rdf:type prov:Agent locally to the current processCandidate.
     * This should be present for every process.
     */
    public addProcessProv(): void {
        this.addProvenanceType(Namespaces.PROV_TYPE.AGENT);
    }

    /**
     * Locally records the authority that is responsible for the process.
     * This should be present for every process and link to the authority that the process exists under.
     * @param authority THe authority object representing the responsible authority.
     */
    public actedOnBehalfOf(authority: Authority<TTrunk>): void {
        this.replaceProvenanceRelation(Namespaces.PROV_RELATION.ACTED_ON_BEHALF_OF, authority.resourceID.resourceURI.toString());
    }
}
