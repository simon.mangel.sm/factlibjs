import {AuthorityConfig} from "./AuthorityConfig";
import {ProcessConfig} from "./ProcessConfig";
import {ActivityConfig} from "./ActivityConfig";

export interface ApplicationConfig
{
    authority: AuthorityConfig;
    process: ProcessConfig;
    activity: ActivityConfig;
}
