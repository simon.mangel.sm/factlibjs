import {LdpResourceID} from "./ldpResourceID";
import {IFactID} from "../../../../datamodels/interfaces/IFactID";

export class LdpFactID extends LdpResourceID implements IFactID {
    private _revisionID: Date | undefined;

    constructor(authorityID: string, resourceID: string, revisionID?: Date) {
        super(authorityID, resourceID);
        this._revisionID = revisionID;
    }

    get factURI(): string {
        const prefix = "fact:" + this.revisionID + ":";
        return prefix + this.resourceURI;
    }

    get revisionID(): string {
        if (this._revisionID) {
            return this._revisionID.toISOString();
        } else {
            return "";
        }
    }

    toString = (): string => {
        return this.factURI;
    }
}
