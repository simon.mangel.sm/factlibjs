import {Namespaces} from "@i5/factlib-utils";
import {MementoLocation} from "./MementoLocation";

// tslint:disable-next-line:no-var-requires
const rdf = require("rdflib");

/**
 * Representation of a Memento timemap associated with a resource as retrieved from a Memento TimeGate.
 */
export class Timemap {
    private _store: any;
    private _uri: URL;

    constructor(store: any, uri: URL) {
        this._store = store;
        this._uri = uri;
    }

    /**
     * Serializes the timemap.
     * @returns A string containing a Turtle serialization of the store.
     */
    public serialize(): string {
        if (this._uri === undefined) {
            return "";
        } else {
            return rdf.serialize(undefined, this.store, this.uri.origin + "/", "text/turtle").split(">.").join("> .");
        }
    }

    /**
     * Retrieves Links to all Mementos of a Resource from the store.
     * @return An Array of URLs pointing to all the Mementos of the resource as indicated in the timemap.
     */
    public getMementoList(): MementoLocation[] {
        const res = this._store.sym(this.resourceURI);
        const rdfResult = this._store.match(res, Namespaces.MEM(Namespaces.MEM_RELATION.MEMENTO));
        const result: MementoLocation[] = [];
        for (const item of rdfResult) {
            const mementoID = this._store.sym(item.object.value);
            const timeobject = this._store.match(mementoID, Namespaces.MEM(Namespaces.MEM_RELATION.DATETIME));
            const datetime = timeobject[0].object.value;
            const url = new URL(item.object.value);
            const date = new Date(datetime);
            const element = {date: date, url: url};
            result.push(element);
        }
        return result;
    }

    get store(): any {
        return this._store;
    }

    get resourceURI(): string {
        return this.uri.origin + this.uri.pathname;
    }

    get uri(): URL {
        return this._uri;
    }

    set uri(value: URL) {
        this._uri = value;
    }
}
