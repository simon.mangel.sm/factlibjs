import {FactDagResource} from "../../../datamodels/factdag/factDagResources/factDagResource";
import {Timemap} from "./resources/timemap";
import {logger, buildLinkHeadersMap, getTimemapFromLinkHeader, TIMEGATE} from "@i5/factlib-utils";
import {AxiosLDPClient} from "@i5/ldpclient";
import {HeadersType, LdpResource} from "@i5/ldpclient";
import {LdpTrunk} from "../resources/ldpTrunk";

export class MementoHandler {
    ldpClient: AxiosLDPClient<LdpResource>;

    constructor(ldpClient: AxiosLDPClient<LdpResource>) {
        this.ldpClient = ldpClient;
    }

    public async getTimemap(resource: FactDagResource<LdpTrunk>): Promise<Timemap> {
        if (resource.trunk) {
            const timeMapUri = resource.trunk.timeMapURI;
            if (timeMapUri) {
                return this.getTimeMapFromURI(timeMapUri);
            } else {
                const timeGateUri = resource.trunk.timeGateURI;
                if (timeGateUri) {
                    return this.getTimeMapFromExternalTimeGate(timeGateUri);
                } else {
                    throw new Error("The resource doesn't support Memento!");
                }
            }
        } else {
            throw new Error("The resource doesn't support Memento!");
        }
    }

    public async getTimeMapFromURI(timeMapUri: URL): Promise<Timemap> {
        const result = await this.ldpClient.getResource(timeMapUri);
        logger.verbose("getTimemap(%s)", timeMapUri.toString());
        return new Timemap(result.store, timeMapUri);
    }

    public async getTimeMapFromExternalTimeGate(timeGateUri: URL): Promise<Timemap> {
        const timeGate = await this.ldpClient.httpHeadResource(timeGateUri);
        const timeMapUri = getTimemapFromLinkHeader(timeGate.headers.link);
        return this.getTimeMapFromURI(new URL(timeMapUri));
    }

    public async getResourceRevisionByDateTimeNegotiation(resourceURI: URL, datetime: Date, isBinary = false, acceptContentType = ""): Promise<LdpResource> {
        try {
            const headers = {
                "accept-datetime" : datetime.toUTCString(), //Correct Serialization?
            };
            const originalResource = await this.getResourceForType(resourceURI, headers, isBinary, acceptContentType);

            const originalMementoDateTime = originalResource.headers["memento-datetime"];
            const originalLinkHeadersMap = buildLinkHeadersMap(originalResource.headers.link);
            if (originalMementoDateTime) {
                return originalResource;
            } else {
                const timeGateURI = originalLinkHeadersMap.getFirstLink(TIMEGATE);
                if (timeGateURI) {
                    const timeGate = await this.getResourceForType(timeGateURI, headers, isBinary, acceptContentType);
                    const timeGateMementoDateTime = originalResource.headers["memento-datetime"];
                    if (timeGateMementoDateTime) {
                        return timeGate;
                    } else {
                        const mementoLocation = timeGate.headers.location;
                        return this.getResourceForType(new URL(mementoLocation), {}, isBinary, acceptContentType);
                    }
                } else {
                    throw new Error("The resource doesn't support Memento!");
                }
            }
        } catch (e) {
            logger.error("Can't get Resource: %s Error: %s", resourceURI.toString(), e);
            throw e;
        }
    }

    private async getResourceForType(uri: URL, headers: HeadersType = {}, isBinary = false, acceptContentType = ""): Promise<LdpResource> {
        if (isBinary) {
            return await this.ldpClient.getLDPNonRDFSource(uri, acceptContentType, headers);
        } else {
            return await this.ldpClient.getResource(uri, headers);
        }
    }

    public async getMementos(mementos: URL[]): Promise<LdpResource[]> {
        const factPromises = mementos.map(async (mementoURL: URL) => {
            const timestamp = mementoURL.searchParams.get("version");
            if (timestamp) {
                try {
                    return await this.ldpClient.getResource(mementoURL);
                } catch (e) {
                    return e;
                }
            }
        });
        return await Promise.all(factPromises);
    }
}
