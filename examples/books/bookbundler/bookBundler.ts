import {FactCandidate, logger, ProcessLifecycleManager, EmptyTrunk, ApplicationConfig} from "../../../index";

const config: ApplicationConfig = require('./config.json');
const rdf = require("rdflib");
const pManager = new ProcessLifecycleManager(config);
const service = pManager.service;
const idFactory = service.getIDFactory();
const factURI = idFactory.createResourceID(config.authority.uri, "facts/bookbundle");
const dep1 = idFactory.createResourceID("http://localhost:8081", "facts/tollesbuch1");
const dep2 = idFactory.createResourceID("http://localhost:8082", "facts/tollesbuch2");
const DC = new rdf.Namespace("http://purl.org/dc/elements/1.1/");

async function updateBundle() {
    try {
        // Retrieve BookFact from local dependency store.
        const bk1 = pManager.dependencies.get(dep1.toString());
        const bk2 = pManager.dependencies.get(dep2.toString());
        if (bk1 && bk2) {
            // Querying the local fact stores to get book prices.
            const rdfResult1 = bk1.queryStore(DC("price"));
            const price1 = Number(rdfResult1[0].object.value);
            const rdfResult2 = bk2.queryStore(DC("price"));
            const price2 = Number(rdfResult2[0].object.value);
            // 10% off! Only today!!
            const bundlePrice = 0.9 * (price1 + price2);
            const isFact = await service.isResourceExistent(factURI);
            let candidate: FactCandidate;
            if (isFact) {
                // If fact exists in the LDP, load it.
                const fact = await service.getLastFactRevision(factURI);
                candidate = fact.createCandidate();
            } else {
                // If fact does not yet exist, create an appropriate Candidate.
                const bundle = "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> . @prefix dc: <http://purl.org/dc/elements/1.1/> . @prefix ex: <http://example.org/stuff/1.0/> . @prefix : <> . : dc:title \"BookBundle\" ; .";
                candidate = new FactCandidate(bundle, idFactory.createResourceID(config.authority.uri, "facts/bookbundle"), EmptyTrunk);
            }
            // Write the calculated bundle price to the local store.
            const res = candidate.store.sym(factURI.toString());
            candidate.store.removeMany(res, DC("price"), null, null, false);
            candidate.store.add(res, DC("price"), String(bundlePrice), res.doc());
            if (isFact) {
                // If fact exists in the LDP, update it.
                await pManager.execProcess([candidate]);
            } else {
                // If fact does not yet exist in the LDP, create it.
                const container = await service.getDirectory(idFactory.createResourceID(config.authority.uri, "facts"));
                await pManager.execProcess([candidate], container);
            }
        } else {
            logger.e("Dependencies not available!");
        }
    } catch (e) {
        logger.error(e);
    }
}

function doThings() {
    // Subscribe to changes in the book facts.
   pManager.start(async (resource) => {
       // Recalculate bundle if a book is changed.
       await updateBundle();
   });
}

doThings();
