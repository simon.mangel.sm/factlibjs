import {FactCandidate, LdpMementoClient, logger, ProcessLifecycleManager, FACTDAG_CLASS} from "../../../index";
import {ApplicationConfig} from "../../../src/datamodels/interfaces/config/ApplicationConfig";

const config: ApplicationConfig = require('./config.json');

// tslint:disable-next-line:no-var-requires
const rdf = require("rdflib");
const pManager = new ProcessLifecycleManager(config);

const service = new LdpMementoClient();
const idFactory = service.getIDFactory();
const DC = new rdf.Namespace("http://purl.org/dc/elements/1.1/");

const authorityID = "http://localhost:8081";
const dep1IId = "facts/tollesbuch1";
const avgIId = "facts/buchavg";

const dep1ID = idFactory.createResourceID(authorityID, dep1IId);
const avgId = idFactory.createResourceID(authorityID, avgIId);

async function calculateAvgPrice() {
    try {
        // Retrieve BookFact from local dependency store.
        const bk1 = pManager.dependencies.get(dep1ID.resourceURI.toString());
        if (bk1) {
            // Get all Facts that are listed in the timemap.
            const mementos = await service.getAllRevisions(bk1, FACTDAG_CLASS.FACT);

            // Calculate the average price by querying the local fact stores.
            let sum = 0;
            let count = mementos.length;
            mementos.forEach((m) => {
                try {
                    const rdfResult = m.queryStore(DC("price"));
                    sum += Number(rdfResult[0].object.value);
                    logger.debug(rdfResult);
                } catch (e) {
                    count--;
                }
            });
            const avgPrice = sum / count;

            const isFact = await service.isResourceExistent(avgId);
            let candidate: FactCandidate;
            if (isFact) {
                // If the fact containing the average price already exists, retrieve it from the LDP.
                const fact = await service.getLastFactRevision(avgId);
                candidate = fact.createCandidate();
            } else {
                // Otherwise create a new FactCandidate containing the appropriate Data.
                const bundle = "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> . @prefix dc: <http://purl.org/dc/elements/1.1/> . @prefix ex: <http://example.org/stuff/1.0/> . @prefix : <> . : dc:title \"Averge book price over time\" ; .";
                candidate = new FactCandidate(bundle, avgId);
            }

            // Write the average price to the local candidate store.
            const res = candidate.store.sym(avgId.toString());
            candidate.store.removeMany(res, DC("price"), null, null, false);
            candidate.store.add(res, DC("price"), String(avgPrice), res.doc());
            if (isFact) {
                // If the fact containing the average price already exists, update it with the candidate.
                await pManager.execProcess([candidate]);
            } else {
                // If the fact does not yet exist, create it.
                const container = await service.getDirectory(idFactory.createResourceID(config.authority.uri, "facts"));
                await pManager.execProcess([candidate], container);
            }
        } else {
            logger.e("Dependencies not available!");
        }

    } catch (e) {
        logger.error("error:" + e);
    }
}

function doThings() {
    // Subscribe to changes in the book fact.
    pManager.start(async (resource: string) => {
        // Recalculate Average Price if the Book-Resource changed.
        logger.debug("Handle changed resources");
        await calculateAvgPrice();
    });
}

doThings();
