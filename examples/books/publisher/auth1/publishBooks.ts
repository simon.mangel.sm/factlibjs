import {
    FactCandidate,
    LdpMementoClient,
    ProcessLifecycleManager,
    ApplicationConfig
} from "../../../../index";

import * as commit from "./commit.json";

const config: ApplicationConfig = require('./config.json');
const rdf = require("rdflib");
const pManager = new ProcessLifecycleManager(config, commit.commit);

const service = new LdpMementoClient();
const idFactory = service.getIDFactory();

const authorityID = "http://localhost:8081";
const internalID = "facts/tollesbuch1";
const resourceID = idFactory.createResourceID(authorityID, internalID);

const DC = new rdf.Namespace("http://purl.org/dc/elements/1.1/");

async function updateBook() {
    // Retrieve the fact from the LDP.
    const fact = await service.getLastFactRevision(resourceID);
    // Query the local store for the book price and increase it by one.
    const rdfResult = fact.queryStore(DC("price"));
    const price = Number(rdfResult[0].object.value) + 1;
    const candidate = fact.createCandidate();
    const res = candidate.store.sym(resourceID.resourceURI.toString());
    // Store updated price in local candidate store.
    candidate.store.removeMany(res, DC("price"), null, null, false);
    candidate.store.add(res, DC("price"), price, res.doc());
    // Create fact revision in LDP
    await pManager.execProcess([candidate]);
}

async function createBook() {
    // Create Candidate with default content.
    const bk1 = "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> . @prefix dc: <http://purl.org/dc/elements/1.1/> . @prefix ex: <http://example.org/stuff/1.0/> . @prefix : <> . : dc:title \"Tolles Buch 1\" ; dc:price 42 .";
    const candidate1 = new FactCandidate(bk1, resourceID);
    const authority = await service.getLastAuthorityRevision(idFactory.createAuthorityResourceID(authorityID));
    candidate1.attributeToAuthority(authority.resourceID);
    const container1 = await service.getDirectory(idFactory.createResourceID(authorityID, "facts"));
    // Create Fact from the created candidate in the container.
    await pManager.execProcess([candidate1], container1);
}

async function doThings() {
    // No subscription needed (no callback given).
    await pManager.start();
    const isFact = await service.isResourceExistent(resourceID);
    if (isFact) {
        // If fact exists in the LDP, update it.
        await updateBook();
    } else {
        // If fact does not yet exist in the LDP, create it.
        await createBook();
    }
}

doThings();
