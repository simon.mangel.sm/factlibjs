import {LdpMementoClient, FactCandidate, ProcessLifecycleManager, ApplicationConfig} from "../../../../index";

const config: ApplicationConfig = require('./config.json');
const rdf = require("rdflib");
const pManager = new ProcessLifecycleManager(config);

const service = new LdpMementoClient();
const idFactory = service.getIDFactory();

const authorityID = "http://localhost:8082";
const internalID = "facts/tollesbuch2";
const resourceID = idFactory.createResourceID(authorityID, internalID);
const DC = new rdf.Namespace("http://purl.org/dc/elements/1.1/");

async function updateBook() {
    const fact = await service.getLastFactRevision(resourceID);
    const rdfResult = fact.queryStore(DC("price"));
    const price = Number(rdfResult[0].object.value) + 2;
    const candidate = fact.createCandidate();
    const res = candidate.store.sym(resourceID.toString());
    candidate.store.removeMany(res, DC("price"), null, null, false);
    candidate.store.add(res, DC("price"), price, res.doc());
    await pManager.execProcess([candidate]);
}

async function createBook() {
    const bk2 = "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> . @prefix dc: <http://purl.org/dc/elements/1.1/> . @prefix ex: <http://example.org/stuff/1.0/> . @prefix : <> . : dc:title \"Tolles Buch 2\" ; dc:price 35 .";
    const candidate2 = new FactCandidate(bk2, resourceID);
    const authority = await service.getLastAuthorityRevision(idFactory.createAuthorityResourceID(authorityID));
    candidate2.attributeToAuthority(authority.resourceID);
    const container2 = await service.getDirectory(idFactory.createResourceID(authorityID, "facts"));
    await pManager.execProcess([candidate2], container2);
}

async function doThings() {
    await pManager.start();
    const isFact = await service.isResourceExistent(resourceID);
    if (isFact) {
        await updateBook();
    } else {
        await createBook();
    }
}

doThings();
